import './utils/ald-stat'
import App from './App'


// import config from "./api/config/index.js"
// Vue.prototype.$config = config
import api from "@/api/apis/index.js";
Vue.prototype.$api = api



// 引入工具类
import * as Tools from './utils/index.js'
Vue.prototype.$Tools = Tools

import store from './store/index.js';
Vue.prototype.$store = store

// main.js
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

// 引入SDK核心类，js文件根据自己业务，位置可自行放置
import QQMapWX from "@/static/qqmap-wx-jssdk/qqmap-wx-jssdk.js"
let qqmapsdk = new QQMapWX({
	key: 'PIABZ-7ELKF-DR2JC-N66HN-W3FNH-VLBNA'
})
Vue.prototype.$qqmapsdk = qqmapsdk


// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	return {
		app
	}
}
// #endif
