
import {
	request
} from "../utils/request"

export default {
	// 首页菜品数据分析
	getFood: (data) => {
		return request({
			url: `/octopus/analysisMonth/food/${data.cityId}/${data.categoryId}`
		})
	},
	// 章鱼数据中心菜品分析
	getFoodList: (data) => {
		return request({
			url: `/octopus/analysisFood/getList/${data.cityId}/${data.categoryId}/${data.month}/${data.type}`
		})
	},
	// 章鱼数据中心-获取对应的建议
	getFoodAdvise: (data) => {
		return request({
			url: `/octopus/analysisMonth/advise/${data.cityId}/${data.month}/${data.categoryId}`
		})
	},
	// 商机评估
	analysisBusinessGet: (data) => {
		return request({
			url: `/octopus/analysisBusiness/get`,
			data
		})
	},
	// 放弃解锁--情报
	informationStudyAbandon: (data) => {
		return request({
			url: `/octopus/informationStudy/abandon/${data.studyId}`
		})
	},
	// 放弃解锁--分析菜品
	foodsStudyAbandon: (data) => {
		return request({
			url: `/octopus/foodsStudy/abandon/${data.studyId}`
		})
	},
	// 品类现状评估【废弃】
	analysisBusinessGetType: (data) => {
		return request({
			url: `/octopus/analysisCategory/get/${data.cityId}/${data.categoryId}`
		})
	},
	// 选址及定价评估
	analysisBusinessGetSite: (data) => {
		return request({
			url: `/octopus/analysisSite/get/${data.cityId}/${data.categoryId}`
		})
	},
	// 对标品牌参考
	analysisBenchmarkGetPPname: (data) => {
		return request({
			url: `/octopus/analysisBenchmark/get/${data.cityId}/${data.categoryId}`,
			data:{
				brandName: data.brandName
			}
		})
	},
	// 菜品详情
	analysisFoodGetDesc: (data) => {
		return request({
			url: `/octopus/analysisFood/get/${data.dataId}`
		})
	},
	// 菜品详情[舍弃]
	foodsGetDesc: (data) => {
		return request({
			url: `/octopus/foods/get/${data.foodId}`
		})
	},
	// 我要学菜品详情
	foodsStudyGet: (data) => {
		return request({
			url: `/octopus/foodsStudy/get/${data.studyId}`
		})
	},
	// 数据图表
	analysisDataGet: (data) => {
		return request({
			url: `/octopus/analysisData/get`,
			data
		})
	},
}
