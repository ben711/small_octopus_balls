import {
	request
} from "../utils/request"

export default {

	// 课程列表
	getCourseList: (data) => {
		return request({
			url: `/octopus/course/getList`,
			data
		})
	},

	// 课程详情
	getCourse: (courseId) => {
		return request({
			url: `/octopus/course/click/${courseId}`
		})
	},

}
