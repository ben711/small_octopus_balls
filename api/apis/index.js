import user from "./user.js";
import isr from "./isr.js";
import order from "./order.js";
import imageText from "./imageText.js";
import community from "./community.js";
import cuisine from "./cuisine.js";
import course from "./course.js";




// 接口对象
var Api = {}



Api = {
	...user,
	...isr,
	...order,
	...imageText,
	...community,
	...cuisine,
	...course,
}


export default Api;
