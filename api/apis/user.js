
import {
	request
} from "../utils/request"

export default {
	// 获取用户信息
	accountWxFnfo: () => {
		return request({
			url: `/octopus/accountWx/info`,
		})
	},
	// 设置当前所在城市
	userLocation: (data) => {
		return request({
			url: `/octopus/user/location`,
			data
		})
	},
	// 完善信息
	accountWxEditInfo: (data) => {
		return request({
			url: `/octopus/accountWx/editInfo`,
			method: 'POST',
			data
		})
	},
	// 添加咨询
	consultAdd: (data) => {
		return request({
			url: `/octopus/consult/add`,
			method: 'POST',
			data
		})
	},
	// 身份列表
	identityGetList: () => {
		return request({
			url: `/octopus/identity/getList`
		})
	},
	// 城市列表
	regionGetList: () => {
		return request({
			url: `/octopus/region/getList`
		})
	},
	// 城市列表2-有设置社群的城市
	regionRegionList: () => {
		return request({
			url: `/octopus/region/regionList`
		})
	},
	// 菜品类别2-根据城市返回对应菜品类别
	getCategoryList: (data) => {
		return request({
			url: `/octopus/foodsCategory/getCategoryList/${data.cityId}`
		})
	},
	// 绑定手机号码
	userWxGetPhone: (data) => {
		return request({
			url: `/octopus/user/wx/getPhone`,
			method: 'POST',
			data
		})
	},
}
