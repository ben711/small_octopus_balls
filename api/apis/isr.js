
import {
	request
} from "../utils/request"

export default {
	// 情报列表
	informationGetList: (data) => {
		return request({
			url: `/octopus/information/getList`,
			data
		})
	},
	// 情报详情
	informationGet: (data) => {
		return request({
			url: `/octopus/information/get/${data.informationId}`,
			data
		})
	},
	// 情报评论列表
	informationCommentGetList: (data) => {
		return request({
			url: `/octopus/informationComment/getList`,
			data
		})
	},
	// 添加评论
	informationCommentAdd: (data) => {
		return request({
			url: `/octopus/informationComment/add`,
			method: 'POST',
			data
		})
	},
	// 点赞操作
	informationLikeAdd: (data) => {
		return request({
			url: `/octopus/informationLike/add/${data.informationId}`
		})
	},
	// 我要学详情
	informationStudyGet: (data) => {
		return request({
			url: `/octopus/informationStudy/get/${data.studyId}`
		})
	},
	// 我的情报
	userInformationGetList: (data) => {
		return request({
			url: `/octopus/userInformation/getList`,
			data
		})
	},
	// 我的课程
	userStudyGetList: (data) => {
		return request({
			url: `/octopus/userStudy/getList`,
			data
		})
	},
	
}
