
import {
	request
} from "../utils/request"

export default {
	// 创建订单
	orderCreate: (data) => {
		return request({
			url: `/octopus/order/create`,
			method: 'POST',
			data
		})
	},
	// 微信支付
	wxPay: (data) => {
		return request({
			url: `/octopus/order/wx/pay/${data.orderNo}`,
			method: 'POST'
		})
	},
	// 订单列表
	orderGetList: (data) => {
		return request({
			url: `/octopus/order/getList`,
			data
		})
	},
	// 支付查询
	orderWxQuery: (data) => {
		return request({
			url: `/octopus/order/wx/query/${data.orderNo}`
		})
	},
	
	
}
