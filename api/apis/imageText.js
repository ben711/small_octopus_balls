
import {
	request
} from "../utils/request"

export default {
	// 解锁社区图文详情 join_community
	// 关于我们图文详情 about_me
	// 首页营销页 marketing_page
	// 大数据定制 custom_made
	operateInfo: (data) => {
		return request({
			url: `/octopus/operate/info`,
			data
		})
	},
	// 每日问候、首页顶部文案、社群宣传文案、社区广告
	contentGet: () => {
		return request({
			url: `/octopus/content/get`
		})
	},
	// 轮播图
	bannerGetList: () => {
		return request({
			url: `/octopus/banner/getList`
		})
	},
	// 社区大咖
	communityGiantGetList: (data) => {
		return request({
			url: `/octopus/communityGiant/getList`,
			data
		})
	},
	
	
}
