import {
	request
} from "../utils/request"

export default {
	// 获取社区价格
	getCity: (data) => {
		return request({
			url: `/octopus/communityPrice/city/${data.cityId}`
		})
	},
	// 邀请折扣价格
	getInvite: (data) => {
		return request({
			url: `/octopus/communityPrice/invite/${data.priceId}`
		})
	},
	// 邀请用户助力
	getAssist: (data) => {
		return request({
			url: `/octopus/communityPrice/assist/${data.masterId}/${data.priceId}`
		})
	},
	// 邀请助力详情
	getInviteXq: (data) => {
		return request({
			url: `/octopus/communityPrice/invite/${data.masterId}/${data.priceId}`
		})
	},
	// 加购福利价
	communityPriceRepurchase: (data) => {
		return request({
			url: `/octopus/communityPrice/repurchase/${data.cityId}`
		})
	},

	// 获取小程序码
	getMpcode: (data) => {
		return request({
			url: `/octopus/communityPrice/assist/share`,
			data,
		})
	},
}
