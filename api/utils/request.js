import config from "../config/index.js"
/**
 * 封装一个异步的请求工具库
 * 基于 uni.request (ajax) 来实现axios的部分功能
 * 
 * 1.调用返回一个promise （以axios举例）
 * 
 * request({
 *  ...配置
 * }).then(res => {}).catch(err => {})
 * 
 * 
 * 2.配置基准路径
 * 
 * request.defaults.baseURL = "路径"
 * 
 * 
 * 3.错误拦截
 * 
 * request.onError(res => {
 *  // 处理错误
 * })
 *
 */
import store from '../../store/index.js'


/*
 * 主函数
 * 
 * @params
 * 参数 | 类型 | 默认值
 * config | Oject | {}
 */
let requestList = [];
let authorization = ''; //token
let is_login = false;








// sessionid过时登录 token错误
const login = cb => {
	
	requestList.push(cb);
	if (!is_login) {
		is_login = true;
	} else {
		return
	}
	

	// 登录请求
uni.login({
		provider: 'weixin',
		success: resLogin => {
			uni.request({
				url: `${request.defaults.baseURL}/auth/wx/login/${resLogin.code}`,
				method: "GET",
				success: res => {
					if (res.data.code === 200) {
						authorization = res.data.data.token;
						// store.commit('updatalocation',res.data.data.location);
						store.commit('updataUser',res.data.data.user);
						// 全部请求回调
						requestList.forEach(cb => {
							cb && cb();
						});
						requestList = [];
						is_login = false;
					};
				}
			});
		}
	});
}
let num = 0;



const request = (config = {}) => {
// 如果url开头没有http，加上基准路径
	if (config.url.search(/^http/) === -1) {
		// 给链接添加url，加上基准路径
		config.url = request.defaults.baseURL + config.url;
	}

	// 返回一个promise
	// resolve是 .then 里面的函数，一般请求成功时候执行
	// reject 是 .catch 里面的函数，一般用于请求失败时候执行
	return new Promise((resolve, reject) => {
		
		uni.showNavigationBarLoading();
		// authorization = uni.getStorageSync('token')



		// 发起请求
		// 判断是否是fromdata数据
	
		let header = {
			Authorization: authorization
		}
		if (config.contentType == "application/x-www-form-urlencoded") {
			header = {
				Authorization: authorization,
				"Content-Type": config.contentType,
			}
		}
		
		
	
		uni.request({
			...config,
			header,
			success(res) {
				if (res.data.code === 401) {
					login(() => {
						// 401
						request(config).then(res => {
							resolve(res);
						});
						
					});
				
					
				
					
				
					
				
				}else if (res.data.code === 500) {
					uni.hideLoading();
					uni.showToast({
						title: '系统繁忙',
						icon: 'none',
						mask: true,
						duration: 2000
					});
					resolve(parseSuccessResponseData(res));
				} else if (res.data.code === 502) {
					uni.hideLoading();
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						mask: true,
						duration: 2000
					});
					resolve(parseSuccessResponseData(res));
				} else if (res.data.code === 503) {
					uni.hideLoading();
					// uni.showToast({
					// 	title: res.data.msg,
					// 	icon: 'none',
					// 	mask: true,
					// 	duration: 2000
					// });
					resolve(parseSuccessResponseData(res));
				} else if (res.data.code === 506) {
					uni.hideLoading();
					uni.showToast({
						title: res.data.msg,
						icon: 'none',
						duration: 2000,
						mask: true,
						success: res => {
							setTimeout(() => {
								uni.navigateBack({
									delta: 1,
								});
							}, 2000);
						}
					});
					resolve(parseSuccessResponseData(res));
				} else if (res.data.code === 508) {
					uni.hideLoading();
					uni.showModal({
						showCancel: false,
						content: res.data.msg
					});
					resolve(res);
				} else {
					// uni.hideLoading();
					// resolve(res);
					resolve(parseSuccessResponseData(res))
				};
			},
			fail(res) {
				reject(res);
			},
			// 不管成功失败都会执行
			complete(res) {
				uni.hideNavigationBarLoading()
				// 执行错误的兰截器
				request.errors(res);
			}
		})
	})


}


// 小程序初始化登录
const firstLogin = () => {
	uni.login({
		provider: 'weixin',
		success: resLogin => {
			uni.request({
				url: `${request.defaults.baseURL}/auth/wx/login/${resLogin.code}`,
				method: "GET",
				success: res => {
					if (res.data.code === 200) {
						authorization = res.data.data.token;
						// store.commit('updatalocation',res.data.data.location);
						store.commit('updataUser',res.data.data.user);
						
					}
				}
			});
		}
	});

}

	
	
	// firstLogin();

/**
 * request的默认属性
 */
request.defaults = {
	// 基准路径
	baseURL: config.url,
	editorURL: 'https://wxapp.bjeesa.cn'
}

/**
 * 存储错误的回调函数.默认是一个空的函数
 */
request.errors = () => {}

/**
 * request的错误拦截
 * 
 * @params
 * callback | 函数 
 */
request.onError = (callback) => {
	// 判断callback必须是一个函数
	if (typeof callback === "function") {
		// 如果是函数，保存到errors
		request.errors = callback
	}
}


const upload = (filePath) => {
	
	// 返回一个promise
	// resolve是 .then 里面的函数，一般请求成功时候执行
	// reject 是 .catch 里面的函数，一般用于请求失败时候执行
	return new Promise((resolve, reject) => {
	
		
		let configDirName = parseInt(Math.random()*100000000000000)
		let config = JSON.parse(JSON.stringify(store.state.ossSignData))
		

		// 发起请求
		uni.uploadFile({
			url: config.host,
			filePath: filePath,
			name: 'file',
			formData: {
				'key':  config.dir  + configDirName +".jpg",
				'policy': config.policy,
				'OSSAccessKeyId': config.accessid,
				'signature': config.signature,
				'success_action_status': '200',
				'callback': config.callback,
				'x:auth': authorization,
				'x:owner_type': 2
			},
			success(res) {
				if (res.statusCode == 200) {
					let {
						code
					} = JSON.parse(res.data);
					if (code === 200) {
						resolve(res);
					} else {
						uni.hideLoading();
						uni.showToast({
							title: '图片违规，请重新上传',
							icon: 'none'
						});
					};
				} else {
					uni.showToast({
						title: '系统报错',
						icon: 'none'
					});
				}

			},
			fail(res) {
				reject(res);
			}
		});
	});
}

const checkUpload = (filePath) => {
	// 返回一个promise
	// resolve是 .then 里面的函数，一般请求成功时候执行
	// reject 是 .catch 里面的函数，一般用于请求失败时候执行
	return new Promise((resolve, reject) => {
		// 发起请求
		uni.uploadFile({
			url: request.defaults.baseURL + '/app/upload/check',
			filePath: filePath,
			name: 'file',
			header: {
				Authorization: authorization,
			},
			success(res) {
				resolve(res);
			},
			fail(res) {
				reject(res);
			}
		});
	});
}

const uploadOSS = (filePath, uploadUrl, formData) => {
	// 返回一个promise
	// resolve是 .then 里面的函数，一般请求成功时候执行
	// reject 是 .catch 里面的函数，一般用于请求失败时候执行
	return new Promise((resolve, reject) => {
		// 发起请求
		uni.uploadFile({
			url: uploadUrl,
			filePath: filePath,
			name: 'file',
			header: {
				Authorization: authorization,
			},
			formData: {
				...formData
			},
			success(res) {
				resolve(res);
			},
			fail(res) {
				reject(res);
			}
		});
	});
}

let globalData = {

}

/* 处理返回的格式*/
function parseSuccessResponseData(res) {
	return res.data || res.result
}

// 暴露
// export default request;
module.exports = {
	request: request,
	upload: upload,
	globalData: globalData,
	checkUpload: checkUpload,
	uploadOSS: uploadOSS
}
