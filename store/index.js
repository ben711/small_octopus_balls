import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);


export default new Vuex.Store({
	state: {
		versions: 'v0.0.0',
		// 微信的胶囊信息 or H5的固定信息
		search: {
			bottom: 80,
			height: 32,
			left: 281,
			right: 368,
			top: 48,
			width: 87
		},
		// 上传图片配置对象
		ossSignData: {},
		// 所在城市
		location: '',
		// 用户信息
		user: null,
		// 身份列表
		identityData: null,
		// 城市列表
		regionData: null,

		imgSrc: 'https://octopus-wxapp.oss-cn-hangzhou.aliyuncs.com/octopus'
	},
	mutations: {
		updataSearch(state, data) {
			state.search = JSON.parse(JSON.stringify(data));
			uni.setStorageSync('search', JSON.stringify(data));
		},
		updataOssSignData(state, data) {
			state.ossSignData = JSON.parse(JSON.stringify(data));
		},
		updatalocation(state, data) {
			state.location = JSON.parse(JSON.stringify(data));
			uni.setStorageSync('location', data);
		},
		updataUser(state, data) {
			state.user = data;
			uni.setStorageSync('user', data);
		},
		updataIdentityData(state, data) {
			state.identityData = data;
		},
		updataRegionData(state, data) {
			state.regionData = data;
		},
	},
	actions: {

	},
	modules: {}
})
