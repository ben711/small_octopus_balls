import config from '../api/config/index.js';

// #ifndef MP-WEIXIN
let wxj = require('jweixin-module');
// #endif
/*
   案例：this.$Tools.deepCapy(a,b);
*/
// 排序 升序
export function rank(arr){
	let nowArr = []
	let startArr = []
	arr.forEach((item, index) => {
		if (item.priceDesc.indexOf("~") != -1) {
			let startNum = item.priceDesc.split('~')[0];
			nowArr.push({
				startNum: Number(startNum),
				...item
			})
		} else if (item.priceDesc.indexOf("<=") != -1 || item.priceDesc.indexOf("<") != -1) {
			startArr.push({
				startNum: 0,
				...item
			})
		} else if (item.priceDesc.indexOf(">=") != -1 || item.priceDesc.indexOf(">") != -1) {
			startArr.push({
				startNum: arr.length,
				...item
			})
		}
	
	})
	
	nowArr.sort((a, b) => a.startNum - b.startNum);
	
	if(startArr.length!=0){
		startArr.forEach(item=>{
			if(item.startNum==0){
				nowArr.unshift(item);
			}else if(item.startNum==arr.length){
				nowArr.push(item);
			}
		})
	}
	return nowArr
}

export function GetSignature(data, callback) {
	// qryWxSignature 这个是调用后台获取签名的接口
	// appId: "wx28656b7f4beb3893"
	// nonceStr: "Zob6yhaNvlaJCRs4"
	// packageValue: "prepay_id=wx26150834523608a8e0ebf34c70efb90000"
	// paySign: "3D537B0028EB6C5C9327C6D7FA33FC0A"
	// signType: "MD5"
	// timeStamp: "1648278514"

	let AppId = data.appId
	let Timestamp = data.timeStamp
	let Signature = data.paySign
	let Noncestr = data.nonceStr

	wxj.config({
		beta: true,
		debug: false,
		appId: AppId,
		timestamp: Timestamp,
		nonceStr: Noncestr,
		signature: Signature,
		// 这里是把所有的方法都写出来了 如果只需要一个方法可以只写一个
		jsApiList: [
			'chooseWXPay',
		]
	})
	wxj.ready(function() {
		wxj.chooseWXPay({
			timestamp: data.timeStamp, // 支付签名时间戳，注意微信jssdk中的所有使用timestamp字段均为小写。但最新版的支付后台生成签名使用的timeStamp字段名需大写其中的S字符
			nonceStr: data.nonceStr, // 支付签名随机串，不长于 32 位
			package: data.packageValue, // 统一支付接口返回的prepay_id参数值，提交格式如：prepay_id=\*\*\*）
			signType: data.signType, // 微信支付V3的传入RSA,微信支付V2的传入格式与V2统一下单的签名格式保持一致
			paySign: data.paySign, // 支付签名
			success: function(res) {
				console.log('支付成功')
				// 支付成功后的回调函数
			}
		});
	})

}

export function myshare(url, desc, imgUrl, shareUserName, shareDesignName) {
	//分享功能
	uni.request({
		url: `${config.url}/mp/config`, //后端接口，返回微信签名串的接口,即上面的Controller
		method: 'GET',
		header: {
			"trade-type": 'JSAPI'
		},
		data: {
			'url': url
		},
		success(res) {
			console.log(res)
			//转换为json对象
			var result = res.data.data;
			//设置分享的标题
			var title = shareUserName;
			wxj.config({
				debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
				appId: result.appId, // 必填，公众号的唯一标识
				timestamp: result.timestamp, // 必填，生成签名的时间戳
				nonceStr: result.nonceStr, // 必填，生成签名的随机串
				signature: result.signature, // 必填，签名
				jsApiList: ["updateAppMessageShareData", "updateTimelineShareData"] // 必填，需要使用的JS接口列表
				/* 即将废弃	jsApiList: ["onMenuShareAppMessage","onMenuShareTimeline"] // 必填，需要使用的JS接口列表 */
			});

			wxj.ready(function() { //需在用户可能点击分享按钮前就先调用
				wxj.updateAppMessageShareData({
					title: title, // 分享标题
					desc: desc, // 分享描述
					link: url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: imgUrl, // 分享图标
					success: function() {

						//alert("分享成功")
						// 设置成功
					},
					fail: function(res) {
						//alert("分享失败")
					},
					cancel: function() {
						// 用户取消分享后执行的回调函数

					}
				});
				wxj.updateTimelineShareData({
					title: title, // 分享标题
					desc: desc, // 分享描述
					link: url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: imgUrl, // 分享图标
					success: function(res) {
						console.log(res.data)

						// 设置成功
					},
					fail: function(res) {
						//alert("分享失败")
					},
					cancel: function() {
						// 用户取消分享后执行的回调函数	 
					}
				})
			});
		}
	})
}

//深拷贝 只能自己封装方法 用递归的方式将旧对象中复合数据类型的属性值 
// 拆为一个一个的基本类型 再一个一个的赋给新对象 对应的属性值,从而达到解除多层引用的关系
//作用：解除多层引用的关系

export function deepCapy(new_obj, old_obj) {
	//先遍历老对象
	for (var Item in old_obj) {
		//将老对象的每一项的属性值暂时存储到main变量中
		var main = old_obj[Item];
		// console.log('main=>',main)

		//然后判断每一项属性值是什么类型
		if (main instanceof Array) { //如果是数组
			// 将对应的新对象属性值 转为 空数组
			new_obj[Item] = [];
			// console.log('Array_main=>',main)
			//用递归 将每一项属性值 类型  为数组 的都给遍历
			deepCapy(new_obj[Item], main);
		} else if (main instanceof Object) { //如果是对象
			// 将对应的新对象属性值 转为 空对象
			new_obj[Item] = {};
			// console.log('Object_main=>',main);
			//用递归 将每一项属性值 类型  为对象 的都给遍历
			deepCapy(new_obj[Item], main);

		} else { //如果是Js的基本类型 null undefined String Number Boolean 
			//因为最后的数据都为基本类型 
			//再赋予对应的属性值
			new_obj[Item] = main;
		}
	}
	//再返回赋值后的新对象
	return new_obj
}

export function handleTime(date, status) {
	date = new Date(date)
	var year = date.getFullYear()
	var month = date.getMonth() + 1
	var day = date.getDate()
	var hour = date.getHours()
	var minute = date.getMinutes()
	// const second = date.getSeconds()
	var str = '';
	if (status === 0) {
		str = '开始'
	} else if (status === 2) {
		str = '结束'
	};

	function formatNumber(n) {
		n = n.toString()
		return n[1] ? n : '0' + n
	}

	return [year, month, day].map(formatNumber).join('.') + ' ' + [hour, minute].map(formatNumber).join(':') + ' ' + str
}



export function formatTime2(date) {
	date = new Date(date)
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()

	// return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
	return [year, month, day].map(formatNumber).join('-')
}

export function formatTime3(date) {
	date = new Date(date)
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = chaDate.getHours()
	const minute = chaDate.getMinutes()
	const second = chaDate.getSeconds()

	return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

export function formatTime(date) {
	date = new Date(date).getTime();
	let nowDate = new Date().getTime();
	let chaDate = date - nowDate;
	const hour = chaDate.getHours()
	const minute = chaDate.getMinutes()
	const second = chaDate.getSeconds()

	return [hour, minute, second].map(formatNumber).join(':')
	// return [year, month, day].map(formatNumber).join('-')
}

export function formatNumber(n) {
	n = n.toString()
	return n[1] ? n : '0' + n
}



// 将图片转换为Base64 callback
export function urlTobase64(img) {
	return new Promise((resolve, reject) => {
		uni.downloadFile({
			url: img,
			success: res => {
				console.log("下载成功1", res)
				uni.getFileSystemManager().readFile({
					filePath: res.tempFilePath, //选择图片返回的相对路径
					encoding: 'base64', //编码格式
					success: resFile => { //成功的回调
						console.log("下载成功", resFile)
						resolve('data:image/png;base64,' + resFile.data)
					}
				})
			}
		})
	})
}

// 修改时间格式
export function setDate(date, type) {
	date = new Date(date);
	let t_ = '.'
	if (type == 1) {
		t_ = '-'
	}
	if (type == 2) {
		t_ = '/'
	}
	return date.getFullYear() + t_ + setHoursMinutes((date.getMonth() + 1)) + t_ + setHoursMinutes(date.getDate()) +
		' ' + setHoursMinutes(date.getHours()) + ':' + setHoursMinutes(date.getMinutes())
}

// 修改时间格式
export function setDateBirthday(date) {
	date = new Date(date);
	let t_ = '-'
	return date.getFullYear() + t_ + setHoursMinutes((date.getMonth() + 1)) + t_ + setHoursMinutes(date.getDate())
}

export function startToEnd(s1, s2) {
	return setDate(s1) + ' - ' + setDate(s2)
}

function setHoursMinutes(e) {
	if (e < 10) {
		return "0" + e
	}
	return e

}

export function setTime(date) {
	date = new Date(date);

	function add0(m) {
		return m < 10 ? "0" + m : m;
	}
	return date.getFullYear() + '-' + add0((date.getMonth() + 1)) + '-' + add0(date.getDate())
}

/**
 * 生成随机或者指定位数的英文数字组合
 * @param {boolean} randomFlag	是否是随机生成位数
 * @param {number} min 			生成随机位数的最小数
 * @param {number} max			生成随机位数的最大数
 * @return {string}				返回生成的英文数字组合
 */
export function randomWord(randomFlag, min, max) {
	let str = '',
		range = min, // 默认赋值为第二个参数，如果是随机产生位数会通过下面的if改变。
		arr = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
		];

	// 随机产生
	if (randomFlag) {
		range = Math.round(Math.random() * (max - min)) + min;
	}
	for (let i = 0; i < range; i++) {
		let index = Math.round(Math.random() * (arr.length - 1));
		str += arr[index];
	}
	return str;
}

// 格式化手机 13437540987 转为 134****0987
export function phoneHidden(phone) {
	return phone.toString().replace(/^(\d{3})\d{4}(\d{4})$/,'$1****$2');
}

// 返回银行卡格式
export function Cardformat(Number = "2021091114204263987972") {
	let newNum = Number.toString().substring(0, Number.length - 4);
	let StrNum = newNum.split('');
	let newArr = [];
	let numStr = "";
	StrNum.forEach((item, index) => {
		numStr = `${numStr}${item}`;
		if (numStr.length == 4 || index + 1 == StrNum.length) {
			newArr.push(numStr);
			numStr = "";
		}
	})
	let Cardformat = newArr.join(' ');
	return Cardformat;
}
